package ca.jsbr.logkt



enum class Color {
    DEFAULT,

    BLACK,
    RED,
    GREEN,
    YELLOW,
    BLUE,
    PURPLE,
    CYAN,
    WHITE;

    companion object {
        val defaultColor = mapOf<LogLevel, ColorStyle>(
            DefaultLevel.DEBUG to ColorStyle(Color.BLUE, Color.DEFAULT, false),
            DefaultLevel.INFO to ColorStyle(Color.DEFAULT, Color.DEFAULT, false),
            DefaultLevel.WARN to ColorStyle(Color.YELLOW, Color.DEFAULT, false),
            DefaultLevel.ERROR to ColorStyle(Color.RED, Color.DEFAULT, false)
        )
    }
}

data class ColorStyle(val color: Color, val background: Color, val bold: Boolean){
    override fun toString(): String {
        return "<$color,$background,$bold>"
    }
}

