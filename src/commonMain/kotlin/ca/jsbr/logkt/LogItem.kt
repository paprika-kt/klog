package ca.jsbr.logkt

interface LogItem {
    fun log(level: LogLevel, messages: MutableMap<String, String>): MutableMap<String, String>?
}