package ca.jsbr.logkt.middleware

import ca.jsbr.logkt.LogLevel

class MinimalPrettyPrint(var formater: (level: LogLevel, messages: MutableMap<String, String>) -> String = ::basicFormatter) : Middleware {
    override fun log(level: LogLevel, messages: MutableMap<String, String>): MutableMap<String, String> {
        val msg = formater(level, messages)
        messages["message"] = msg
        return messages
    }
}

