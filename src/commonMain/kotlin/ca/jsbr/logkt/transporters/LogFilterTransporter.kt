package ca.jsbr.logkt.transporters

import ca.jsbr.logkt.LogLevel

class LogFilterTransporter(val map: Map<String, LogLevel>, private val key: String = "context") : Transporter {

    constructor(vararg pair: Pair<String, LogLevel>) : this(pair.toMap())
    constructor(key: String, vararg pair: Pair<String, LogLevel>) : this(pair.toMap(), key)

    override fun log(level: LogLevel, messages: MutableMap<String, String>): MutableMap<String, String>? {
        if ((map[messages[key]]?.value ?: 0f) > level.value)
            return null
        return messages
    }
}