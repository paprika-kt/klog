package ca.jsbr.logkt.transporters

import ca.jsbr.logkt.LogLevel

class BasicConsoleTransporter : Transporter {


    override fun log(level: LogLevel, messages: MutableMap<String, String>): MutableMap<String, String> {
        println(messages["message"])
        return messages
    }
}
