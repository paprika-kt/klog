package ca.jsbr.logkt

interface Logger: LogItem{
    fun add(vararg logItem: LogItem)
    fun add(vararg logItem: LogItem, index:Int)
    fun add(at: Int, logItem: LogItem)
}

