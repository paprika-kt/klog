@file:Suppress("unused")

package ca.jsbr.logkt

import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

class LoggerDelegate(private val parent: Logger = klog, private vararg val pair: Pair<String, String> = arrayOf()) : ReadOnlyProperty<Any, Logger> {
    private var logger: Logger? = null
    override operator fun getValue(thisRef: Any, property: KProperty<*>): Logger {
        var name = thisRef::class.toString().split(" ")[1]
        name = name.split(".").takeLast(4).joinToString(".")
        logger = logger ?: parent.child(
            "context" to name,
            *pair)
        return logger!!
    }
}

fun Logger.context(vararg pair: Pair<String, String> = arrayOf()): LoggerDelegate =
    LoggerDelegate(this, *pair)

