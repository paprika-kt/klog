package ca.jsbr.logkt.Utils

import ca.jsbr.logkt.*
import kotlin.error

fun <T> Logger.logNull(level: LogLevel, message: String): T? {
    log(level, message)
    return null
}

fun <T> Logger.debugNull(message: String) = logNull<T>(DefaultLevel.DEBUG, message)
fun <T> Logger.infoNull(message: String) = logNull<T>(DefaultLevel.INFO, message)
fun <T> Logger.warnNull(message: String) = logNull<T>(DefaultLevel.WARN, message)
fun Logger.fail(message: String): Nothing {
    log(DefaultLevel.ERROR, message)
    kotlin.error(message)
}
