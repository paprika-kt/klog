package ca.jsbr.logkt

enum class DefaultLevel(override val value: Float, override val label: String) : LogLevel {
    DEBUG(1f, "DEBUG"),
    INFO(2f, "INFO"),
    WARN(3f, "WARN"),
    ERROR(4f, "ERROR");

    override val mappedLevel: DefaultLevel
        get() = this
}