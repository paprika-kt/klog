package ca.jsbr.logkt

interface LogLevel {
    val value: Float
    val label: String
    val mappedLevel: DefaultLevel
}