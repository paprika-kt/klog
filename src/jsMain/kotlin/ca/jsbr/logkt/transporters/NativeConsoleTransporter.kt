package ca.jsbr.logkt.transporters

import ca.jsbr.logkt.Color
import ca.jsbr.logkt.ColorStyle
import ca.jsbr.logkt.DefaultLevel
import ca.jsbr.logkt.LogLevel
import ca.jsbr.logkt.Utils.ConsoleColors
import kotlin.browser.window

val isBrowser = jsTypeOf(window) != undefined

actual class NativeConsoleTransporter : Transporter {

    actual var styles: Map<LogLevel, ColorStyle> = Color.defaultColor
    actual var enableColor: Boolean = true

    override fun log(level: LogLevel, messages: MutableMap<String, String>): MutableMap<String, String> {

        var msg = messages["message"]
        var style = ""
        if (enableColor) {
            style = if (isBrowser) {
                msg = "%c$msg"
                getBrowserStyle(level)
            } else getNodeStyle(level)
        }
        when (level.mappedLevel) {
            DefaultLevel.DEBUG -> console.log(msg, style)
            DefaultLevel.INFO -> console.log(msg, style)
            DefaultLevel.WARN -> console.warn(msg, style)
            DefaultLevel.ERROR -> console.error(msg, style)
        }
        return messages
    }

    private fun getBrowserStyle(level: LogLevel): String {
        val style = styles[level] ?: styles[level.mappedLevel] ?: return ""
        var result = ""
        if (style.color == Color.YELLOW && level.mappedLevel == DefaultLevel.WARN)
            result += "color:GoldenRod; "
        else if (style.color != Color.DEFAULT)
            result += "color:${style.color.name.toLowerCase()}; "
        if (style.background != Color.DEFAULT)
            result += "background-color:${style.color.name.toLowerCase()}; "
        if (style.bold)
            result += "font-weight=\"bold\"; "


        return result
    }

    private fun getNodeStyle(level: LogLevel): String {
        val style = styles[level] ?: styles[level.mappedLevel] ?: return ""
        var result = ConsoleColors.CLEAR.unix
        result += if (style.bold) ConsoleColors.valueOf(style.color.name + "_BOLD").unix else ConsoleColors.valueOf(
            style.color.name
        ).unix
        result += ConsoleColors.valueOf(style.background.name + "_BACKGROUND").unix
        return result
    }

}